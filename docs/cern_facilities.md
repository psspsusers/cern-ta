# CERN Facilities in EURO-LABS


Within EURO-LABS, [CERN](http://cern.ch) offers Trans-National Access (TNA) to 9 facilities covering the full spectrum of physics of the Laboratory. <br />
Details in each facility, such as beam types, energies, particle spieces, performance figures as well as possible experiments can be found on their specific pages listed below. 


- WP2 - Access to RIs for Physics

| **Facility** | **Coordinator** | **e-mail** |
|:---------------:| :---------------:|:---------------:|
| [ISOLDE](https://isolde.web.cern.ch) | Sean Freeman | [Sean.Freeman@cern.ch](mailto:Sean.Freeman@cern.ch) |
| [nTOF](https://ntof-exp.web.cern.ch) | Alberto Mengoni | [Alberto.Mengoni@cern.ch](mailto:Alberto.Mengoni@cern.ch) |

-  WP3 - Access to RIs for Accelerator R&d

| **Facility** | **Coordinator** | **e-mail** |
|:------------:|:---------------:|:---------------:|
| [HiRadMat](https://hiradmat.web.cern.ch) | Nikos Charitonidis | [Nikos.Charitonidis@cern.ch](mailto:Nikos.Charitonidis@cern.ch) | 
| [XBOX](https://aries.web.cern.ch/xbox) | Roberto Corsini  | [Roberto.Corsini@cern.ch](mailto:Roberto.Corsini@cern.ch) | 
| [CLEAR](https://clear.cern) |  Roberto Corsini  | [Roberto.Corsini@cern.ch](mailto:Roberto.Corsini@cern.ch) | 

- WP4 - Access to RIs for HEP Detector R&D

| **Facility** | **Coordinator** | **e-mail** |
|:------------:|:---------------:|:---------------:|
| Test beams in [PS, SPS ](https://ps-sps-coordination.web.cern.ch/ps-sps-coordination/) <br> and [world-wide](https://test-beam-facilities.web.cern.ch/) | Eva Barbara Holzer | [barbara.holzer@cern.ch](mailto:barbara.holzer@cern.ch) |
| [IRRAD](https://ps-irrad.web.cern.ch/ps-irrad/) | Federico Ravotti | [Federico.Ravotti@cern.ch](mailto:Federico.Ravotti@cern.ch) |
| [GIF++](https://ep-dep-dt.web.cern.ch/irradiation-facilities/gif) | Michael Moll | [Michael.Moll@cern.ch)](mailto:Michael.Moll@cern.ch) | 

